# JAVA, Maven, POO

Le rendu  de ce TP se fera sous la forme d'un dépôt Git. 

Vous serez évalué sur la qualité générale du code, ainsi que sur les tests unitaires et la documentation associés. 

## 1 - Introduction à Maven 

La première partie de ce TP à pour but de vous familiariser avec l'utilisation de Maven. 

1- Commencez par télécharger et installer Maven : https://maven.apache.org/

En vous basant sur les explications du tutoriel ["Getting Started" de la documentation de Maven](https://maven.apache.org/guides/getting-started/) :

2- Créez un projet

3- Compilez le projet

4- Générez un livrable

5- Essayez d'exécuter le livrable. Vous devriez avoir une erreur. 

L'archétype Maven de base permet de générer des livrables, mais le manifest de ceux-ci est incomplet. 

Pour résoudre ce problème, nous allons utiliser le plugin Maven Assembly Plugin, qui permet la gestion de la création des livrables. 

6- Ajoutez le plugin [Maven Assembly Plugin](https://maven.apache.org/plugins/maven-assembly-plugin/usage.html).
> Utilisez le `descriptorRef` "jar-with-dependencies"
> Attention, ce plugin doit être ajouté dans la section `build > plugins` et non `build > pluginsManagement`

7- Utilisez la commande `mvn package` pour générer les livrables à nouveau. Un nouveau livrable suffixé de "jar-with-dependencies" devrait être créé. Exécutez le avec la commande `java -jar` et vérifiez que vous voyez bien le "Hello World"

## 2 - POO / Design Patterns 

Récupérez en local le Squelette du projet. 

```sh
git clone https://gitlab.com/raphael.marzat/java-maven-but
```

Ce projet contient les tests unitaires de la partie 2.1, ainsi que les classes principales. 

Dans les ressources, vous trouverez les données du pokédex de 1ère génération, réparties dans 3 fichiers (2 fichiers JSON, 1 fichier XML).

### 2.1 - Lecture des données depuis les fichiers

Dans un premier temps, le but de cet exercice est de lire ces fichiers et d'importer les données du Pokédex dans votre programme. 

Nous avons ici deux types de fichiers dans lesquels il faut récupérer des données. 

Nous souhaitons mettre en oeuvre une solution permettant à la classe principale de pouvoir faire appel à un service permettant la lecture des différents fichiers de manière transparente. 

Le but est de pouvoir intégrer de nouveaux formats de fichiers à l'avenir de la manière la plus simple possible.

*Exemple d'utilisation souhaitée :*

```java
public class PokemonDataManipulation {
    public static void main(String[] args) {
        FileReaderService fileReaderService = new FileReaderService();
        MyData myData1 = fileReaderService.readFile("myFile.json", MyData);
        MyData myData2 = fileReaderService.readFile("myFile.xml", MyData);
        
        // Demain, on pourrait être amené à vouloir lire les fichiers CSV
        // MyData myData3 = fileReaderService.readFile("myFile.csv", MyData);
    }
}
```

Pour cela, vous pouvez créer des classes bas-niveau permettant d'effectuer la lecture des données. 
La classe `FileReaderService`, s'occupera quant à elle de déterminer lequel des lecteurs appeler pour un type de fichier donné.  

Appuyez-vous sur des bibliothèques externes permettant la conversion des différents types de données en objets Java.

### 2.2 - Manipulation des données

1- Implémentez les 3 méthodes présentes dans la classe `PokemonStatisticsService`. 

2- Ajoutez les méthodes `findHeaviestPokemonFunctionnal`, `findPokemonsByTypeFunctionnal` et `groupPokemonsByTypeFunctionnal` qui font la même chose, en utilisant la programmation fonctionnelle. (Voir API Stream)

---

## Améliorations 

* Depuis Java 14, il est possible de créer des classes de données immutables dont l'utilisation supprime beaucoup de code passe-plat : les `records`. Ici, nous ne faisons que de la lecture et de la manipulation sur les données existantes sans les modifier donc l'utilisation de records est appropriée.  
Transformez votre classe de modèle de données en record. 

* Ajoutez des logs dans votre application. Pour cela, vous pourez vous baser sur logback ainsi que log4j. 
