package fr.univ.larochelle.service;

import fr.univ.larochelle.exceptions.FormatNotHandledException;
import fr.univ.larochelle.exceptions.UnparsableDataException;
import fr.univ.larochelle.model.PokemonData;
import fr.univ.larochelle.model.PokemonDataList;
import fr.univ.larochelle.service.model.CarModel;
import fr.univ.larochelle.service.model.CarModelList;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class FileReaderServiceTest {

    private FileReaderService fileReaderService = new FileReaderService();

    @Test
    public void readFile_JSON() {
        //When
        PokemonDataList pokemonDataList = fileReaderService.readFile("src/test/resources/pokedex_sample.json", PokemonDataList.class);
        PokemonData pokemonData = pokemonDataList.getPokedexDataList().get(0);

        //Then
        assertThat(pokemonDataList.getPokedexDataList().size(), is(3));
        assertThat(pokemonData.getId(), is(1L));
        assertThat(pokemonData.getNum(), is("001"));
        assertThat(pokemonData.getName(), is("Bulbasaur"));
        assertTrue(pokemonData.getType().contains("Grass"));
        assertThat(pokemonData.getHeight(), is(0.71F));
        assertThat(pokemonData.getWeight(), is(6.9F));
    }

    @Test
    public void readFile_XML(){
        //When
        PokemonDataList pokemonDataList = fileReaderService.readFile("src/test/resources/pokedex_sample.xml", PokemonDataList.class);
        PokemonData pokemonData = pokemonDataList.getPokedexDataList().get(0);

        //Then
        assertThat(pokemonDataList.getPokedexDataList().size(), is(3));
        assertThat(pokemonData.getId(), is(4L));
        assertThat(pokemonData.getNum(), is("004"));
        assertThat(pokemonData.getName(), is("Charmander"));
        assertTrue(pokemonData.getType().contains("Fire"));
        assertThat(pokemonData.getHeight(), is(0.61F));
        assertThat(pokemonData.getWeight(), is(8.5F));
    }

    @Test(expected = FormatNotHandledException.class)
    public void readFile_FormatNotHandled(){
        fileReaderService.readFile("src/test/resources/car_models.csv", PokemonDataList.class);
    }

    @Test(expected = UnparsableDataException.class)
    public void readFile_DataParsingError(){
        fileReaderService.readFile("src/test/resources/pokedex_sample.xml", PokemonData.class);
    }
}