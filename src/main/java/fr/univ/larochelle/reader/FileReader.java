package fr.univ.larochelle.reader;

public interface FileReader {

    /**
     * Takes the path of a file as an input and returns parsed content as a Java Object
     * @param filePath The path of the file containing the data
     * @param T The Class of the Java Object to be parsed
     * @return The Java Object corresponding to the data in the file
     */
    <T> T readData(String filePath, Class T);
}
