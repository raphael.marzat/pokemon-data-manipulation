package fr.univ.larochelle.service;

import fr.univ.larochelle.model.PokemonData;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class PokemonStatisticsService {

    public PokemonData findHeaviestPokemon(List<PokemonData> pokemonDataList){
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public Set<PokemonData> findPokemonsByType(List<PokemonData> pokemonDataList, String type){
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public Map<String, PokemonData> groupPokemonsByType(List<PokemonData> pokemonDataList){
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
